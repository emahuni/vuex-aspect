import Aspect from "./classes/Aspect";

export default install;

/**
 * @public
 * Install vuex-aspect onto the given vuex store instance.
 *
 * @param {*} store The vuex store instance.
 * @returns {*} The store.
 */
function install(store) {
  prepareAspectsOf(store, store._modules.root, "");
  const modMap = store._modulesNamespaceMap;
  for (let ns in modMap) { prepareAspectsOf(store, modMap[ns], ns); }
  return store;
}

function prepareAspectsOf(store, module, ns) {
  let aspects = module._rawModule.aspects;
  if (Array.isArray(aspects)) {
    for (let i = 0; i < aspects.length; i++) {
      iteratee(aspects[i], i, aspects, true);
    }
  } else if (typeof aspects === "object" && aspects !== null) {
    for (let key in aspects) {
      // noinspection JSUnfilteredForInLoop
      iteratee(aspects[key], key, aspects);
    }
  }

  function iteratee(aspect, key, collection, isArray) {
    if (!(aspect instanceof Aspect)) { aspect = collection[key] = new Aspect(aspect); }
    if (aspect.id != null) { throw new Error("Aspect for " + ns + key + " has already been bound to " + aspect.id); }
    if (isArray && aspect._optionKey == null) { throw new Error("Aspect #" + key + " for " + ns + " has no key."); }
    aspect._bind(ns, key, store, module);
  }
}
