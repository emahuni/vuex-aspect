/**
 * Creates mixins for the given aspect instance.
 * Mixins are aliases for common use-cases (e.g. vue instance lifecycle grasp/release, navigation-based grasp/release).
 *
 * @param {Aspect} aspect The aspect instance.
 * @returns {{life,route}} The created mixins.
 */
export default function mixins(aspect) {

  let grasp = aspect.grasp.bind(aspect);
  let release = aspect.release.bind(aspect);

  return {

    life: {
      // regular vue components
      created: grasp,
      destroyed: release,
      // keep-alive vue components
      activated: grasp,
      deactivated: release,
    },

    route: {
      // vue-router compatible navigation guards
      beforeRouteEnter: (to, from, next) => {
        aspect
          .grasp()
          .then(next, next);
      },
      beforeRouteLeave: (to, from, next) => {
        aspect.release();
        next();
      },
    },

  };
}
