// this file does not contain all options since some have no default value or have programmatic default values

export default {
  enable() { },
  supply() { },
  disable() { this.clear(); },
  error({reason, isDropped}) {
    // eslint-disable-next-line no-console
    console.error(reason);
    isDropped || this.clearCache();
  },
};
