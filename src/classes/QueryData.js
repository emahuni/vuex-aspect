/**
 * A factory class for query data objects. Query data objects are description objects of some query containing a
 * consecutive identification number (identifies only within one instance of {@link QueryData}).
 * Further this class is responsible for all mutations on the query data objects.
 */
export default class QueryData {

  /**
   * @public
   * Creates a new QueryData factory. Use {@link QueryData#next} to create query data objects for each query.
   *
   * @constructor
   */
  constructor() {
    this.latestId = -1;
    this.latestReceivedId = -1;
  }

  /**
   * @public
   * Checks whether the query has been out-raced by another query (race-condition).
   *
   * @param {Object} queryData The query data object that has been created with {@link QueryData#next}.
   * @returns {Boolean} Whether the query should be dropped.
   */
  checkDrop(queryData) {
    queryData.isDropped = queryData.id < this.latestReceivedId;
    if (!queryData.isDropped) {
      this.latestReceivedId = queryData.id;
      queryData.isLatest = this.latestId === queryData.id;
    }
    return queryData.isDropped;
  }

  /**
   * @public
   * Creates a new query data object.
   *
   * @returns {Object} The new query data.
   */
  next() {
    return {
      id: ++this.latestId,

      // following additional values are set conditionally...

      // query trigger flags:
      // isClear: false,
      // isInjection: false,

      // query parameters:
      // variables: void 0,

      // query return status:
      // hasFailed: false,
      // isDropped: false,
      // isLatest: false,

      // query return value:
      // value: void 0,
      // reason: void 0,
    };
  }

  /**
   * @public
   * Attaches the given variables to the query data object.
   *
   * @param {Object} queryData The query data.
   * @param {*} variables The variables to attach.
   * @returns {Object} The query data.
   */
  static withVariables(queryData, variables) {
    queryData.variables = variables;
    return queryData;
  }

  /**
   * @public
   * Marks a query data object as triggered by clear action.
   *
   * @param {Object} queryData The query data.
   * @returns {Object} The query data.
   */
  static clearTriggered(queryData) {
    queryData.isClear = true;
    return queryData;
  }

  /**
   * @public
   * Marks a query data object as triggered by injection action.
   *
   * @param {Object} queryData The query data.
   * @returns {Object} The query data.
   */
  static injectionTriggered(queryData) {
    queryData.isInjection = true;
    return queryData;
  }

  /**
   * @public
   * Attaches the provided value to the query data object as success value.
   *
   * @param {Object} queryData The query data object.
   * @param {*} value The success value of the query.
   * @returns {Object} The mutated query data object.
   */
  static onSuccess(queryData, value) {
    queryData.value = value;
    queryData.hasFailed = false;
    return queryData;
  }

  /**
   * @public
   * Attaches the provided value to the query data object as failure reason.
   *
   * @param {Object} queryData The query data object.
   * @param {*} reason The failure reason of the query.
   * @returns {Object} The mutated query data object.
   */
  static onError(queryData, reason) {
    queryData.reason = reason;
    queryData.hasFailed = true;
    return queryData;
  }

}
