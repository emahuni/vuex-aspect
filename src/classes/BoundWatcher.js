/**
 * A controller class for the state of watching reactive state. It controls the variables collected from the reactive
 * state and provides methods for enabling and disabling the watcher.
 */
export default class BoundWatcher {

  /**
   * @public
   * Creates a new bound watcher (inactive by default) for an aspect.
   *
   * @param {Aspect} aspect The aspect instance. The variablesFn is called with it as `this`.
   * @param {Function} updateFn A function to call whenever the (reactive) variables change.
   * @param {Function<*>|*|null} variablesFn The function to collect variables; or the variables itself.
   * @param {Object} options Options for vm.$watch (immediate option is always set to true).
   * @constructor
   */
  constructor(aspect, updateFn, variablesFn, options) {
    this.aspect = aspect;
    this.options = options || {};

    this.updateFn = updateFn;
    this.reactive = typeof variablesFn === "function";
    this.enabled = false;
    this.variables = this.reactive ? void 0 : variablesFn;
    this.oldVariables = this.variables;
    this._stop = void 0;

    this.variablesFn = this.reactive ? variablesFn : void 0;

    this.__collectVariables = this._collectVariables.bind(this);

    this.options.immediate = true; // immediate ensures that this._trigger is called synchronously
  }

  /**
   * @public
   * Disables the watcher if it is enabled. Removes the cached variables as well.
   */
  stop() {
    if (!this.enabled) { throw new Error("BoundWatcher not enabled."); }
    this.enabled = false;
    if (!this.reactive) { return; }
    this._stop();
    this._stop = void 0;
    this.variables = void 0;
    this.oldVariables = void 0;
  }

  /**
   * @public
   * Starts the actual watcher (if reactive) with immediate (synchronous) variable collection.
   *
   * @returns {*|null} The collected variables.
   */
  enable() {
    if (this.enabled) { throw new Error("BoundWatcher already enabled."); }
    this.enabled = true;
    if (!this.reactive) {
      this.updateFn({variables: this.variables, oldVariables: this.oldVariables, isInitial: true});
      return this.variables;
    }
    let isInitial = true;
    this._stop = this.aspect.$store.watch( // passed methods called synchronously due to immediate option
      this.__collectVariables,
      (variables, oldVariables) => {
        this.variables = variables;
        this.oldVariables = oldVariables;
        this.updateFn({variables, oldVariables, isInitial});
        isInitial = false;
      },
      this.options,
    );
    return this.variables;
  }

  /**
   * @private
   * Called by watcher whenever the state changes.
   *
   * @param {*} rootState The vuex root state.
   * @param {*} rootGetters The vuex root getters.
   * @returns {*} The collected variables.
   */
  _collectVariables(rootState, rootGetters) {
    const ctx = this.aspect.$module;
    return this.variablesFn.call(this.aspect, {state: ctx.state, getters: ctx.getters, rootState, rootGetters});
  }

}
