/**
 * Runs a specified mutation operation.
 * This function operates with (private-indicated) vuex internal functions. This may cause trouble when vuex internals
 * change. We should consider adding a similar public function upstream.
 *
 * @param {*} store The vuex store object.
 * @param {{type, payload}} mutation The mutation specification (passed to all subscribers, e.g. development tools).
 * @param {Function} fn The operation function.
 */
export default function commit(store, mutation, fn) {
  const state = store.state;
  store._withCommit(fn);
  if (mutation != null) { store._subscribers.forEach(sub => sub(mutation, state)); }
}
