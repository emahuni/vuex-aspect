import install from "./src/install";
import Aspect from "./src/classes/Aspect";

export {Aspect, install};
