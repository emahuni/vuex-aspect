# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [2.0.0] - 2018-04-22

### Changed
- Remove default export in favor of `install`.
- Remove deprecated `context` parameter for resolve.
- Remove deprecated `Aspect#forceFetch` method.

### Added
- Export `install` function.

### Fixed
- `Aspect#grasp`, if not the first call, did not await the initial query and thus did not properly ensure the values being set on the state (mistaken `Aspect#_isCleared`) [#8](https://gitlab.com/frissdiegurke/vuex-aspect/issues/8)

## [1.1.0] - 2018-04-21

### Added
- `Aspect#context` function to create a similar context object to what vuex actions get passed.
- [major] The `Aspect` constructor now accepts some `{Function} supply` option that gets called whenever (new) variables are
  ready. This is intended to be used for subscription-based aspects (See [/examples/subscription](https://gitlab.com/frissdiegurke/vuex-aspect/tree/master/examples/subscription)) [#6](https://gitlab.com/frissdiegurke/vuex-aspect/issues/6)

### Deprecated
- The `resolve` constructor option functions 2nd parameter `context`. Use `this.context()` instead.

### Fixed
- Renamed considered private constructor option values on the `Aspect` instance to `Aspect#_option*`

## [1.0.2] - 2018-04-19

### Added
- Additional options for vm.$watch can now be passed as `watch` parameter to the `Aspect` constructor.
- The `resolve` constructor option is now optional. If not provided, the state won't change automatically.

### Fixed
- Using Aspects without vuex namespace and having `store.replaceState` called previously caused the Aspect to commit to the old state (and thus not being attached to the view) [#7](https://gitlab.com/frissdiegurke/vuex-aspect/issues/7)

## [1.0.1] - 2018-04-17

### Fixed
- Loading indicates `false` despite outstanding queries [#5](https://gitlab.com/frissdiegurke/vuex-aspect/issues/5)

## [1.0.0] - 2018-04-16

### Changed
- `Aspect#key` now holds a short-relevant initialization key if provided via options
- The `error` option function now receives the query data object instead of the reason only. The `latest` parameter is not passed anymore; use `!isDropped` instead.
- Renamed the query data object attribute `error` to `reason`, `isInject` to `isInjection` and `isDrop` to `isDropped`
- Some `Aspect` attributes have been made private

### Removed
- `Aspect#key`, `Aspect#errorKey`, `Aspect#loadingKey`

### Added
- Browser ready distribution files
- `clearCache` parameter for `Aspect#fetch`
- Aspect configuration is now sufficient within vuex modules; instantiation is now handled on-the-fly

### Deprecated
- `VuexAspect.prepareStore` use `VuexAspect` instead
- `Aspect#forceFetch` use `Aspect#fetch(true)` instead

## [0.3.3] - 2018-04-12

### Added
- Extend `context` parameter for resolve option by `rootGetters` and `rootState`

## [0.3.2] - 2018-04-11

### Changed
- Use MIT license

## [0.3.1] - 2018-04-11

### Added
- Export prepareStore as default

## [0.3.0] - 2018-04-11

### Changed
- Rename package to `vuex-aspect`
- Rename `Aspect#value` to `Aspect#cache`
- Rename subscription type (append `/success`)
- Add vuex mutations `#plugins/vue-aspect/initialize` and `#plugins/vue-aspect/error`
- `Aspect#fetch` now uses the cache as well.
- `Aspect#fetch` now always returns a promise of the newly crafted payload object
- Default `error` option does not throw anymore, instead it removes itself from the cache
- Options `error()`, `disable()` and `enable()` are not treated asynchronous anymore
- `Aspect#release` is not async anymore
- `Aspect#grasp` no longer returns the payload
- Default values from the store will suffice and prevent fetch without variable change

### Added
- Allow `variables` to be of any type, not just function
- Add `key$loading` and `key$error` attributes to state
- Initialize `state[key]` if not existent for proper vue.js watching behavior
- `Aspect#inject(value, error)`
- `Aspect#clearCache()`
- `error` option now accepts a second parameter `{boolean} latest`
- `Aspect#lifeMixin` is an alias of `Aspect#mixin`
- `Aspect#routeMixin` (alias: `Aspect#guards`) contains appropriate navigation guards
- `Aspect#forceFetch()`
- Ensure that each aspect is only bound to one module

### Removed
- `Aspect#setValue`, use `Aspect#inject` instead
- Possibility to skip via `false` return of variables option

## [0.2.0] - 2018-04-04

### Changed
- Use created instead of mounted hook for mixin
- Rename `cleanup` option to `disable`

### Added
- Add activated and deactivated hook within mixin
- Add `Aspect#constructor` option `enable`
- Add `Aspect#clear`, `Aspect#setValue`, `Aspect#setCache` methods
- Throw on `consumers < 0`

### Fixed
- Some `await null` evaluations of the cache

## [0.1.2] - 2018-04-04

### Added
- Update repository link

## [0.1.1] - 2018-04-04

### Added
- Repository link
- Improve README.md and package.json

## [0.1.0] - 2018-04-04

### Added
- Basic application
